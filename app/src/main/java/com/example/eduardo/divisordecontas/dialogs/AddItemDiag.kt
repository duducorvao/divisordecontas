package com.example.eduardo.divisordecontas.dialogs

import android.app.AlertDialog
import android.app.Dialog
import android.app.DialogFragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import com.example.eduardo.divisordecontas.R
import com.example.eduardo.divisordecontas.database.ItemDAO
import com.example.eduardo.divisordecontas.entity.Item

class AddItemDiag : DialogFragment(){

    var dialogRef: Dialog? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val layoutInf: LayoutInflater = activity.layoutInflater
        val alertDiagBuilder = AlertDialog.Builder(activity)
        val layoutView: View = layoutInf.inflate(R.layout.simple_name_price_dialog, null)

        alertDiagBuilder.setView(layoutView)
        alertDiagBuilder.setTitle("Criar Item")

        alertDiagBuilder.setNegativeButton("Cancel", { dialog, which -> cancelButtonClick()})
        alertDiagBuilder.setPositiveButton("Ok", {dialog, which -> okButtonClick() })

        val newDialog = alertDiagBuilder.create()

        dialogRef = newDialog

        return newDialog
    }

    fun okButtonClick(){
        //Toast.makeText(activity.baseContext, dialogRef?.findViewById<TextView>(R.id.editText_nome)?.text , Toast.LENGTH_SHORT).show()
        val itemDao: ItemDAO = ItemDAO(activity)

        var itemName = dialogRef?.findViewById<TextView>(R.id.editText_nome)?.text
        var itemPrice = dialogRef?.findViewById<TextView>(R.id.editText_preco)?.text

        val newRest = Item(itemName.toString(), itemPrice.toString().toFloat())

        itemDao.insert(newRest)
    }

    fun cancelButtonClick(){
        dismiss()
    }
}