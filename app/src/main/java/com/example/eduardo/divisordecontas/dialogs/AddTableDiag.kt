package com.example.eduardo.divisordecontas.dialogs

import android.app.AlertDialog
import android.app.Dialog
import android.app.DialogFragment
import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.widget.Toast
import com.example.eduardo.divisordecontas.database.RestauranteDAO
import com.example.eduardo.divisordecontas.entity.Restaurant

class AddTableDiag : DialogFragment() {

    var dialogRef: Dialog? = null
    lateinit var restDAO: RestauranteDAO
    lateinit var allRest: MutableList<Restaurant>

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val layoutInf: LayoutInflater = activity.layoutInflater
        val alertDiagBuilder = AlertDialog.Builder(activity)
        //val layoutView: View = layoutInf.inflate(R.layout.recycle_row, null)

        restDAO = RestauranteDAO(activity)
        allRest = restDAO.findAll() as MutableList<Restaurant>

        var allRestNames: MutableList<CharSequence> = arrayListOf()

        for (rest in allRest){
            allRestNames.add(rest.name)
        }

        alertDiagBuilder.setItems(allRestNames.toTypedArray(), DialogInterface.OnClickListener { dialogInterface: DialogInterface, i: Int ->
            okButtonClick(i)
        })

        alertDiagBuilder.setTitle("Abrir Mesa")

        alertDiagBuilder.setNegativeButton("Cancel", { dialog, which -> cancelButtonClick()})
        alertDiagBuilder.setPositiveButton("Ok", {dialog, which -> okButtonClick(-1) })

        val newDialog = alertDiagBuilder.create()

        dialogRef = newDialog

        return newDialog
    }


    fun okButtonClick(itemPosition: Int){

        if(itemPosition >= 0){ //Chamada veio da seleção do restaurante



        }

//        val restDao: RestauranteDAO = RestauranteDAO(activity)
//
//        var restName = dialogRef?.findViewById<TextView>(R.id.editText_nome)?.text
//
//        val newRest = Restaurant(restName.toString())
//
//        restDao.insert(newRest)
        //Toast.makeText(activity, itemPosition.toString(), Toast.LENGTH_SHORT).show()

    }

    fun cancelButtonClick(){
        dismiss()
    }

}