package com.example.eduardo.divisordecontas.fragments

import android.app.Fragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.eduardo.divisordecontas.R

/**
 * Created by Eduardo on 28-Nov-17.
 */
class Fragment2 : Fragment() {

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater!!.inflate(R.layout.create_item_dialog_frag, container,false)
    }

}