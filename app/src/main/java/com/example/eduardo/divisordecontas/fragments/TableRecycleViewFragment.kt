package com.example.eduardo.divisordecontas.fragments

import android.annotation.TargetApi
import android.app.Fragment
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.eduardo.divisordecontas.R
import com.example.eduardo.divisordecontas.database.ServingTableDAO
import com.example.eduardo.divisordecontas.entity.Person
import com.example.eduardo.divisordecontas.entity.ServingTable
import com.example.eduardo.divisordecontas.entity.TableOrder
import com.example.eduardo.divisordecontas.recycleviewer.TableRecycleAdapter

class TableRecycleViewFragment : Fragment() {

    lateinit var tableAdapter: TableRecycleAdapter
    lateinit var tableDAO: ServingTableDAO
    lateinit var firstOpenTable: ServingTable
    lateinit var allOrderList: MutableList<TableOrder>

    @TargetApi(Build.VERSION_CODES.M)
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {

        tableDAO = ServingTableDAO(activity)

        var layout: View = inflater!!.inflate(R.layout.recycle_fragment, container, false) as View

        var allItemsList: MutableList<ServingTable> = tableDAO.findAll() as MutableList<ServingTable>

        if(allItemsList.size != 0){
            firstOpenTable = allItemsList[0]
        }else{
            firstOpenTable = ServingTable()
        }

        allOrderList = firstOpenTable.order

        tableAdapter = TableRecycleAdapter(activity, allOrderList, this)

        var itemRecycleView = layout.findViewById<RecyclerView>(R.id.recyclerRef)

        itemRecycleView.adapter = tableAdapter
        itemRecycleView.layoutManager = LinearLayoutManager(activity)

        return layout
    }

    private fun GetAllTables(context: Context): ArrayList<ServingTable> {
        val tableDAO = ServingTableDAO(context)

        return tableDAO.findAll() as ArrayList<ServingTable>
    }

    fun RemoveTableOrderFromDB(table: TableOrder) {

        firstOpenTable.order.remove(table)

        var servingTableDAO: ServingTableDAO = ServingTableDAO(activity)
        servingTableDAO.update(firstOpenTable)

    }
}