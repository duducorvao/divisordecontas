package com.example.eduardo.divisordecontas.dialogs

import android.app.AlertDialog
import android.app.Dialog
import android.app.DialogFragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import com.example.eduardo.divisordecontas.R

class OpenTableDiag : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val layoutInf: LayoutInflater = activity.layoutInflater
        val alertDiagBuilder = AlertDialog.Builder(activity)
        val layoutView: View = layoutInf.inflate(R.layout.simple_name_dialog, null)

        alertDiagBuilder.setView(layoutView)
        alertDiagBuilder.setTitle("Abrir Mesa")
        alertDiagBuilder.setMessage("Selecione o Restaurante")

        alertDiagBuilder.setNegativeButton("Cancel", { dialog, which -> cancelButtonClick()})
        alertDiagBuilder.setPositiveButton("Ok", {dialog, which -> okButtonClick() })


        return alertDiagBuilder.create()
    }

    fun okButtonClick(){

    }

    fun cancelButtonClick(){

    }

}