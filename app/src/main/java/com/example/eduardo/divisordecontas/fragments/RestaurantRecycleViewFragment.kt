package com.example.eduardo.divisordecontas.fragments

import android.annotation.TargetApi
import android.app.Fragment
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.eduardo.divisordecontas.R
import com.example.eduardo.divisordecontas.database.RestauranteDAO
import com.example.eduardo.divisordecontas.entity.Restaurant
import com.example.eduardo.divisordecontas.recycleviewer.RestaurantRecycleAdapter

class RestaurantRecycleViewFragment : Fragment() {

    lateinit private var restAdapter: RestaurantRecycleAdapter
    lateinit var restDAO: RestauranteDAO

    @TargetApi(Build.VERSION_CODES.M)
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {

        restDAO = RestauranteDAO(activity)

        var layout: View = inflater!!.inflate(R.layout.recycle_fragment, container, false) as View

        var allRestList: MutableList<Restaurant> = restDAO.findAll() as MutableList<Restaurant>

        restAdapter = RestaurantRecycleAdapter(activity, allRestList, this)

        var restRecycleView = layout.findViewById<RecyclerView>(R.id.recyclerRef)

        restRecycleView.adapter = restAdapter
        restRecycleView.layoutManager = LinearLayoutManager(activity)

        return layout
    }


    private fun GetAllRestaurants(context: Context): ArrayList<Restaurant>{
        val restDAO = RestauranteDAO(context)

        return restDAO.findAll() as ArrayList<Restaurant>
    }

    fun RemoveRestaurantFromDB(rest: Restaurant){
        restDAO.delete(rest)
    }

    fun EditRestaurantOnDB(rest: Restaurant){
        restDAO.update(rest)
    }
}