package com.example.eduardo.divisordecontas.database

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import com.example.eduardo.divisordecontas.entity.Item
import com.example.eduardo.divisordecontas.entity.Person
import com.example.eduardo.divisordecontas.entity.ServingTable
import com.example.eduardo.divisordecontas.entity.TableOrder

class ServingTableDAO(context: Context) : GenericDAO<ServingTable>(context, "servingtable") {

    override fun getContentValuesFromEntity(obj: ServingTable): ContentValues {
        val contentValues = ContentValues()

        contentValues.put("id", obj.id)

        return contentValues

    }

    override fun createEntityFromCursor(cursor: Cursor): ServingTable {

        val searchedId = cursor.getLong(cursor.getColumnIndex("_id"))

        if(searchedId != null){
            return ServingTable(
                    searchedId,
                    findAllServingTablePerson(searchedId),
                    findAllServingTableItems(searchedId),
                    mutableListOf()
            )
        }
        else{
            return ServingTable()
        }

    }

    private fun findAllServingTableItems(servingTableId: Long):List<Item>{

        val list = ArrayList<Item>()
        val itemDAO: ItemDAO = ItemDAO(context)

        val cursor = db.query(
                "servingtable_item",
                null,
                "servingtable_id = ?",
                arrayOf(servingTableId.toString()),
                null,
                null,
                "servingtable_id ASC",
                null)

        if (cursor.count > 0) {

            cursor.moveToFirst()
            do {
                list.add(itemDAO.createEntityFromCursor(cursor))
            } while (cursor.moveToNext())

        }

        return list

    }

    private fun findAllServingTablePerson(servingTableId: Long):List<Person>{

        val list = ArrayList<Person>()
        val personDAO: PersonDAO = PersonDAO(context)

        val cursor = db.query(
                "servingtable_person",
                null,
                "servingtable_id = ?",
                arrayOf(servingTableId.toString()),
                null,
                null,
                "servingtable_id ASC",
                null)

        if (cursor.count > 0) {

            cursor.moveToFirst()
            do {
                list.add(personDAO.createEntityFromCursor(cursor))
            } while (cursor.moveToNext())

        }

        return list

    }

}