package com.example.eduardo.divisordecontas.database

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log

class AppDatabase(val context: Context): SQLiteOpenHelper(context, "divisor_de_contas.db", null, 1) {

    override fun onCreate(db: SQLiteDatabase) {
        executeSQL(db, "db/divisor_database_create.sql")
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        executeSQL(db, "db/divisor_database_update.sql")
    }

    private fun executeSQL(db: SQLiteDatabase, path: String) {
        val sql = context.assets.open(path)
        sql.bufferedReader().forEachLine {
            if (!it.trim().isEmpty()){
                Log.d("STATE", it)
                db.execSQL(it)
            }
        }
    }

}