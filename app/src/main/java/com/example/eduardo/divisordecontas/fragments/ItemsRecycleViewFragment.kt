package com.example.eduardo.divisordecontas.fragments

import android.annotation.TargetApi
import android.app.Fragment
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.eduardo.divisordecontas.R
import com.example.eduardo.divisordecontas.database.ItemDAO
import com.example.eduardo.divisordecontas.entity.Item
import com.example.eduardo.divisordecontas.recycleviewer.ItemRecycleAdapter

class ItemsRecycleViewFragment: Fragment() {

    lateinit private var itemAdapter: ItemRecycleAdapter
    lateinit var itemDAO: ItemDAO

    @TargetApi(Build.VERSION_CODES.M)
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {

        itemDAO = ItemDAO(activity)

        var layout: View = inflater!!.inflate(R.layout.recycle_fragment, container, false) as View

        var allItemsList: MutableList<Item> = itemDAO.findAll() as MutableList<Item>

        itemAdapter = ItemRecycleAdapter(activity, allItemsList, this)

        var itemRecycleView = layout.findViewById<RecyclerView>(R.id.recyclerRef)

        itemRecycleView.adapter = itemAdapter
        itemRecycleView.layoutManager = LinearLayoutManager(activity)

        return layout
    }

    private fun GetAllItems(context: Context): ArrayList<Item>{
        val itemDAO = ItemDAO(context)

        return itemDAO.findAll() as ArrayList<Item>
    }

    fun RemoveItemFromDB(item: Item){
        itemDAO.delete(item)
    }

}