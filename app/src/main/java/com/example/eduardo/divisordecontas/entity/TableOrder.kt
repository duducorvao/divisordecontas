package com.example.eduardo.divisordecontas.entity

data class TableOrder(override val id: Long, val person: Person, val item: Item ) : IEntity {
    constructor(person: Person, item: Item) : this(0, person, item)
}