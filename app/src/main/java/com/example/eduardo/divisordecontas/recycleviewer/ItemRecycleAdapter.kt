package com.example.eduardo.divisordecontas.recycleviewer

import android.app.Activity
import android.app.DialogFragment
import android.content.Context
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.example.eduardo.divisordecontas.R
import com.example.eduardo.divisordecontas.dialogs.NewNameDialog
import com.example.eduardo.divisordecontas.entity.Item
import com.example.eduardo.divisordecontas.fragments.ItemsRecycleViewFragment
import org.w3c.dom.Text

class ItemRecycleAdapter(val context: Context, val data: MutableList<Item>, val fragmentRef: ItemsRecycleViewFragment): RecyclerView.Adapter<ItemRecycleAdapter.MyViewHolder>() {

    private var inflater: LayoutInflater?

    init {
        inflater = LayoutInflater.from(context)
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): MyViewHolder {
        var newView: View = inflater?.inflate(R.layout.recycle_row_item, parent, false) as View
        var holder:MyViewHolder = MyViewHolder(newView)

        return holder

    }

    override fun onBindViewHolder(holder: MyViewHolder?, position: Int) {
        val currentItem: Item = data[position]
        holder?.ViewPosition = position
        holder?.rowText?.text = currentItem.name
        holder?.rowPrice?.text = currentItem.price.toString()
    }

    override fun getItemCount(): Int {
        return data.size
    }

    inner class MyViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        var ViewPosition: Int = 0
        var cardRef: CardView = itemView!!.findViewById(R.id.cardview_row)
        var deleteBtn: Button = itemView!!.findViewById(R.id.apagar_btn)
        var rowText: TextView = itemView!!.findViewById(R.id.row_text)
        var rowPrice: TextView = itemView!!.findViewById(R.id.row_price)

        init {
            cardRef.setOnClickListener(this)
            deleteBtn.setOnClickListener(this)
        }

        override fun onClick(p0: View?) {
            when(p0?.id){
                R.id.cardview_row -> CardViewBtnEvent(p0)
                R.id.apagar_btn -> DeleteBtnEvent(p0)
            }
        }

        fun CardViewBtnEvent(cardView: View?){
            Toast.makeText(cardView?.context, "Pressed Card", Toast.LENGTH_SHORT).show()
        }

        fun EditBtnEvent(btnView: View?){
            //Toast.makeText(btnView?.context, "Pressed Editar", Toast.LENGTH_SHORT).show()
            //val editDialog = NewNameDialog()
            //editDialog.show(, "NoticeDialogFragment")

        }

        fun DeleteBtnEvent(btnView: View?){
            //remover do banco
            fragmentRef.RemoveItemFromDB(data[ViewPosition])

            data.removeAt(ViewPosition)
            notifyItemRemoved(ViewPosition)
            notifyItemRangeChanged(ViewPosition, data.size)
        }

    }

}