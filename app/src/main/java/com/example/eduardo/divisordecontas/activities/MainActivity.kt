package com.example.eduardo.divisordecontas.activities

import android.app.Activity
import android.app.FragmentTransaction
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.design.widget.Snackbar
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.view.MenuItem
import android.widget.TextView
import com.example.eduardo.divisordecontas.R
import com.example.eduardo.divisordecontas.database.AppDatabase
import com.example.eduardo.divisordecontas.database.RestauranteDAO
import com.example.eduardo.divisordecontas.dialogs.AddItemDiag
import com.example.eduardo.divisordecontas.dialogs.AddPersonDiag
import com.example.eduardo.divisordecontas.dialogs.AddRestDiag
import com.example.eduardo.divisordecontas.dialogs.AddTableDiag
import com.example.eduardo.divisordecontas.entity.Item
import com.example.eduardo.divisordecontas.entity.Person
import com.example.eduardo.divisordecontas.entity.TableOrder
import com.example.eduardo.divisordecontas.fragments.ItemsRecycleViewFragment
import com.example.eduardo.divisordecontas.fragments.PersonRecycleViewFragment
import com.example.eduardo.divisordecontas.fragments.RestaurantRecycleViewFragment
import com.example.eduardo.divisordecontas.fragments.TableRecycleViewFragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import java.util.*

//AppCompatActivity()
class MainActivity : Activity(), NavigationView.OnNavigationItemSelectedListener {

    val dataBaseRef: AppDatabase = AppDatabase(this)
    var hasChosenRest: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //setSupportActionBar(toolbar)

        //Floating Action Button
        fab.setOnClickListener { view ->
            Snackbar.make(view, "Nenhuma aba selecionada", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
        }

        //Adiciona o botão do navigation drawer no canto superior esquerdo.
        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)
    }

    //Fecha o navigation drawer
    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

//    override fun onCreateOptionsMenu(menu: Menu): Boolean {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        menuInflater.inflate(R.menu.main, menu)
//        return true
//    }
//
//    override fun onOptionsItemSelected(item: MenuItem): Boolean {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        when (item.itemId) {
//            R.id.action_settings -> return true
//            else -> return super.onOptionsItemSelected(item)
//        }
//    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {

            R.id.nav_view_table -> {

                fab.setOnClickListener { view -> AddTableDiag().show(fragmentManager, "CustomDialog")
                }

                val transaction:FragmentTransaction = fragmentManager.beginTransaction()
                transaction.replace(R.id.fragment_container, TableRecycleViewFragment())
                transaction.addToBackStack(null)
                transaction.commit()

            }

            R.id.nav_view_restaurant -> {

                //Floating Action Button
                fab.setOnClickListener { view -> AddRestDiag().show(fragmentManager, "CustomDialog")
                }

                val transaction:FragmentTransaction = fragmentManager.beginTransaction()
                transaction.replace(R.id.fragment_container, RestaurantRecycleViewFragment())
                transaction.addToBackStack(null)
                transaction.commit()
            }

            R.id.nav_view_items -> {

                //Floating Action Button
                fab.setOnClickListener { view -> AddItemDiag().show(fragmentManager, "CustomDialog")
                }

                val transaction:FragmentTransaction = fragmentManager.beginTransaction()
                transaction.replace(R.id.fragment_container, ItemsRecycleViewFragment())
                transaction.addToBackStack(null)
                transaction.commit()
            }

            R.id.nav_view_person -> {
                //Floating Action Button
                fab.setOnClickListener { view -> AddPersonDiag().show(fragmentManager, "CustomDialog")
                }

                val transaction:FragmentTransaction = fragmentManager.beginTransaction()
                transaction.replace(R.id.fragment_container, PersonRecycleViewFragment())
                transaction.addToBackStack(null)
                transaction.commit()
            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }
}
