package com.example.eduardo.divisordecontas.entity

interface IEntity {
    val id: Long
}