package com.example.eduardo.divisordecontas.dialogs
import android.app.AlertDialog
import android.app.Dialog
import android.app.DialogFragment
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import android.widget.Toast
import com.example.eduardo.divisordecontas.R
import com.example.eduardo.divisordecontas.database.PersonDAO
import com.example.eduardo.divisordecontas.database.RestauranteDAO
import com.example.eduardo.divisordecontas.entity.Person
import com.example.eduardo.divisordecontas.entity.Restaurant

@RequiresApi(Build.VERSION_CODES.M)
class AddRestDiag: DialogFragment() {

    var dialogRef: Dialog? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val layoutInf:LayoutInflater = activity.layoutInflater
        val alertDiagBuilder = AlertDialog.Builder(activity)
        val layoutView:View  = layoutInf.inflate(R.layout.simple_name_dialog, null)

        alertDiagBuilder.setView(layoutView)
        alertDiagBuilder.setTitle("Criar Restaurante")

        alertDiagBuilder.setNegativeButton("Cancel", { dialog, which -> cancelButtonClick()})
        alertDiagBuilder.setPositiveButton("Ok", {dialog, which -> okButtonClick() })

        val newDialog = alertDiagBuilder.create()

        dialogRef = newDialog

        return newDialog
    }


    fun okButtonClick(){
        val restDao: RestauranteDAO = RestauranteDAO(activity)

        var restName = dialogRef?.findViewById<TextView>(R.id.editText_nome)?.text

        val newRest = Restaurant(restName.toString())

        restDao.insert(newRest)
    }

    fun cancelButtonClick(){
        dismiss()
    }

}