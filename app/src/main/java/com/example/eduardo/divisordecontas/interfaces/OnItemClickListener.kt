package com.example.eduardo.divisordecontas.interfaces

import android.view.View

interface OnItemClickListener {
    fun onItemClick(view: View, position: Int)
}