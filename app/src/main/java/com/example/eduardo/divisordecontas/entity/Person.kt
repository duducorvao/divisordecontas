package com.example.eduardo.divisordecontas.entity

data class Person(override val id: Long, val name: String) : IEntity{
    constructor(name: String) : this(0, name)
}