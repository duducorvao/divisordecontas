package com.example.eduardo.divisordecontas.entity

data class ServingTable(override val id: Long, val person: List<Person>, val item: List<Item>, val order: MutableList<TableOrder> ) : IEntity {
    constructor(id: Long): this(id, ArrayList<Person>(), ArrayList<Item>(), ArrayList<TableOrder>())
    constructor(): this(0, ArrayList<Person>(), ArrayList<Item>(), ArrayList<TableOrder>())
}