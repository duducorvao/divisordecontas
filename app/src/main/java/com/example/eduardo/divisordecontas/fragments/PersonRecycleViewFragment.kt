package com.example.eduardo.divisordecontas.fragments

import android.annotation.TargetApi
import android.app.Fragment
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.eduardo.divisordecontas.R
import com.example.eduardo.divisordecontas.database.PersonDAO
import com.example.eduardo.divisordecontas.entity.Person
import com.example.eduardo.divisordecontas.recycleviewer.PersonRecycleAdapter


class PersonRecycleViewFragment : Fragment() {

    lateinit private var personAdapter: PersonRecycleAdapter
    lateinit var personDAO: PersonDAO

    @TargetApi(Build.VERSION_CODES.M)
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {

        personDAO = PersonDAO(activity)

        var layout: View = inflater!!.inflate(R.layout.recycle_fragment, container, false) as View

        var allPersonList: MutableList<Person> = personDAO.findAll() as MutableList<Person>

        personAdapter = PersonRecycleAdapter(activity, allPersonList, this)

        var itemRecycleView = layout.findViewById<RecyclerView>(R.id.recyclerRef)

        itemRecycleView.adapter = personAdapter
        itemRecycleView.layoutManager = LinearLayoutManager(activity)

        return layout
    }

    private fun GetAllPerson(context: Context): ArrayList<Person>{
        val personDAO = PersonDAO(context)

        return personDAO.findAll() as ArrayList<Person>
    }

    fun RemovePersonFromDB(person: Person){
        personDAO.delete(person)
    }

}