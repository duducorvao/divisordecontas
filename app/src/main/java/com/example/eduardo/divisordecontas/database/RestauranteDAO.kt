package com.example.eduardo.divisordecontas.database

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import com.example.eduardo.divisordecontas.entity.Item
import com.example.eduardo.divisordecontas.entity.Restaurant

class RestauranteDAO(context: Context) : GenericDAO<Restaurant>(context, "restaurant") {

    override fun getContentValuesFromEntity(obj: Restaurant): ContentValues {
        val contentValues = ContentValues()

        contentValues.put("name", obj.name)

        return contentValues
    }

    override fun createEntityFromCursor(cursor: Cursor): Restaurant {
//        val servingTableDAO = ServingTableDAO(context)
//
//        val servingTable = servingTableDAO.find(cursor.getLong((cursor.getColumnIndex("servingtable_id"))))

        return Restaurant(
                cursor.getLong(cursor.getColumnIndex("_id")),
                cursor.getString(cursor.getColumnIndex("name")),
                findAllRestaurantItems(cursor.getLong(cursor.getColumnIndex("_id")))
        )

    }

    private fun findAllRestaurantItems(restaurantId: Long):List<Item>{

        val list = ArrayList<Item>()
        val itemDAO: ItemDAO = ItemDAO(context)

        val cursor = db.query(
                "restaurant_item",
                null,
                "restaurant_id = ?",
                arrayOf(restaurantId.toString()),
                null,
                null,
                "restaurant_id ASC",
                null)

        if (cursor.count > 0) {

            cursor.moveToFirst()
            do {
                list.add(itemDAO.createEntityFromCursor(cursor))
            } while (cursor.moveToNext())

        }

        return list

    }


}