package com.example.eduardo.divisordecontas.database

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import com.example.eduardo.divisordecontas.entity.Person

class PersonDAO(context: Context) : GenericDAO<Person>(context, "person") {

    override fun getContentValuesFromEntity(obj: Person): ContentValues {
        val contentValues = ContentValues()

        contentValues.put("name", obj.name)

        return contentValues

    }

    override fun createEntityFromCursor(cursor: Cursor): Person {

        return Person(
                cursor.getLong(cursor.getColumnIndex("_id")),
                cursor.getString(cursor.getColumnIndex("name"))
        )
    }


}