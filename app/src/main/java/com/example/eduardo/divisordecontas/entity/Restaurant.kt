package com.example.eduardo.divisordecontas.entity

data class Restaurant(override val id: Long, val name: String, val items: List<Item>) : IEntity{
    constructor(name: String, items: List<Item> ) : this(0, name, items)
    constructor(name: String) : this(0, name, ArrayList<Item>() )
}