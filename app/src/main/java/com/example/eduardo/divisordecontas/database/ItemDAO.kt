package com.example.eduardo.divisordecontas.database

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import com.example.eduardo.divisordecontas.entity.Item

class ItemDAO(context: Context) : GenericDAO<Item>(context, "item") {

    override fun getContentValuesFromEntity(obj: Item): ContentValues {
        val contentValues = ContentValues()

        contentValues.put("name", obj.name)
        contentValues.put("price", obj.price)

        return contentValues

    }

    override fun createEntityFromCursor(cursor: Cursor): Item {

        return Item(
                cursor.getLong(cursor.getColumnIndex("_id")),
                cursor.getString(cursor.getColumnIndex("name")),
                cursor.getFloat(cursor.getColumnIndex("price"))
        )

    }
}