package com.example.eduardo.divisordecontas.entity

data class Item(override val id: Long, val name: String, val price: Float) : IEntity{
    constructor(name: String, price: Float) : this(0, name, price)
    constructor(name: String) : this(0, name, 0.0f)

}