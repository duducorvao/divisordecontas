package com.example.eduardo.divisordecontas.dialogs

import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.app.DialogFragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import com.example.eduardo.divisordecontas.R
import android.content.Context


class NewNameDialog : DialogFragment() {

    interface NoticeDialogListener {
        fun onDialogPositiveClick(dialog: DialogFragment)
        fun onDialogNegativeClick(dialog: DialogFragment)
    }

    var mListener: NoticeDialogListener? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val layoutInf: LayoutInflater = activity.layoutInflater
        val alertDiagBuilder = AlertDialog.Builder(activity)
        val layoutView: View = layoutInf.inflate(R.layout.simple_name_dialog, null)

        alertDiagBuilder.setView(layoutView)
        alertDiagBuilder.setTitle("Novo nome")

        alertDiagBuilder.setNegativeButton("Cancel", { dialog, which -> cancelButtonClick()})
        alertDiagBuilder.setPositiveButton("Ok", {dialog, which -> okButtonClick() })


        return alertDiagBuilder.create()
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)

        mListener = activity as NoticeDialogListener

//        // Verify that the host activity implements the callback interface
//        try {
//            // Instantiate the NoticeDialogListener so we can send events to the host
//
//        } catch (e: ClassCastException) {
//            // The activity doesn't implement the interface, throw exception
//            throw ClassCastException(activity.toString() + " must implement NoticeDialogListener")
//        }
    }

    fun okButtonClick(){

        //dismiss()
    }

    fun cancelButtonClick(){

        //dismiss()
    }
}