DROP TABLE IF EXISTS restaurant;
DROP TABLE IF EXISTS servingtable;
DROP TABLE IF EXISTS person;
DROP TABLE IF EXISTS item;
DROP TABLE IF EXISTS restaurant_item;
DROP TABLE IF EXISTS servingtable_item;
DROP TABLE IF EXISTS servingtable_person;

